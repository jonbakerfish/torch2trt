import torch
from torch2trt.module_test import add_module_test

import detectron2
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog


################ mask_rcnn_X_101_32x8d_FPN_3x ##################
def get_mask_rcnn_X_101_32x8d_FPN_3x():
    cfg = get_cfg()
    cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")
    predictor = DefaultPredictor(cfg)
    model = predictor.model
    return model

# config: https://github.com/facebookresearch/detectron2/blob/master/configs/COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml
# weight: https://dl.fbaipublicfiles.com/detectron2/COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x/139653917/model_final_2d9806.pkl
# python3 -m torch2trt.test -o test.out --name mask_rcnn_X_101_32x8d_FPN_3x --include=torch2trt.tests.detectron2.models
@add_module_test(torch.float32, torch.device('cuda'), [(1, 3, 800, 1024)], fp16_mode=False)
def mask_rcnn_X_101_32x8d_FPN_3x_backbone_bottom_up():
    model = get_mask_rcnn_X_101_32x8d_FPN_3x()
    net = model.backbone.bottom_up
    return net

# @add_module_test(torch.float16, torch.device('cuda'), [(1, 3, 224, 224)], fp16_mode=True)
# def mask_rcnn_X_101_32x8d_FPN_3x_backbone_bottom_up_16():
#     model = get_mask_rcnn_X_101_32x8d_FPN_3x()
#     net = model.backbone.bottom_up
#     return net
